
Python For Android Tasks
========================

Python for android invoke tasks specification.

Installation
============

To install p4atasks, clone the project and run the setup.py script. The following line works on Linux, other OS not tested:

    sudo ./setup.py install


License
=======

See LICENSE file